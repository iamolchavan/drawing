import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import IconButton from './IconButton';

class Header extends React.Component {

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.buttonsContainer}> 
          <View style={{flex:4, marginLeft:20}}>
            <IconButton
              disabled={this.props.isRemoveEnable}
              onPress={this.props.cancel}
              name="remove"
              image = {require('../assets/remove.png')}
            />
          </View>                   
          <View style={[styles.buttonsContainer, {flex:5}]}>
            <IconButton
              disabled={this.props.isUndoEnable}
              onPress={this.props.undo}
              name="undo"
              image = {require('../assets/undo.png')}
            />
            <IconButton
              disabled={this.props.isRedoEnable}
              onPress={this.props.redo}
              name="redo"
              image = {require('../assets/redo.png')}
              />
            <IconButton
              disabled={this.props.isDoneEnable}
              onPress={this.props.save}
              name="Done"
              image = ''
            />
          </View>         
          
          
        </View>
      </View>
    );
  }
}

let styles = StyleSheet.create({
  container: {
    flex:1,
    flexDirection: 'column',
    backgroundColor: '#EEE',
  },

  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    flex:1,
  },

  title: {
    color: '#444',
    fontSize: 20,
    fontWeight: '900',    
    flex:1,
  }
});

export default Header;
