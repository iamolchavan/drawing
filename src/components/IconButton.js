import React from 'react';
import { StyleSheet, TouchableOpacity, Text,Image } from 'react-native';
// import { FontAwesome } from '@exponent/vector-icons';

class IconButton extends React.Component {

  render() {
    return (
      <TouchableOpacity
        disabled={this.props.disabled}
        style={styles.iconButton}
        onPress={this.props.onPress}
      >{
        this.props.image?
        <Image style={{width:30,height:30}} source={this.props.image}></Image>
        :<Text>{this.props.name}</Text>
        }
      </TouchableOpacity>
    );
  }
}

let styles = StyleSheet.create({
  icon: {
    color: '#999',
    fontSize: 22,
    margin: 5
  }
});

export default IconButton;
