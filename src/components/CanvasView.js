
import React from 'react';
import { Dimensions, ImageBackground, View, PanResponder, StyleSheet } from 'react-native';
import Svg, { G, Path } from 'react-native-svg';
import Drawing from './Drawing';
const screenWidth = Math.round(Dimensions.get('window').width)
const screenHeight = Math.round(Dimensions.get('window').height)


export default class CanvasView extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      currentMax: 0,
      currentPoints: [],
      drawing: new Drawing(),
      donePaths: []
    };

    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gs) => true,
      onMoveShouldSetPanResponder: (evt, gs) => true,
      onPanResponderGrant: (evt, gs) => this.onResponderGrant(evt, gs),
      onPanResponderMove: (evt, gs) => this.onResponderMove(evt, gs),
      onPanResponderRelease: (evt, gs) => this.onResponderRelease(evt, gs)
    });
  }

  onTouch(evt) {
    let [x, y] = [evt.nativeEvent.pageX, evt.nativeEvent.pageY];
    const newCurrentPoints = this.state.currentPoints;
    newCurrentPoints.push({ x, y });

    this.setState({
      donePaths: [...this.state.donePaths, ...this.props.donePaths],
      currentPoints: newCurrentPoints,
      currentMax: this.state.currentMax
    });
  }

  onResponderGrant(evt) {
    this.onTouch(evt);
  }

  onResponderMove(evt) {
    this.onTouch(evt);
  }

  onResponderRelease() {
    const newPaths = this.props.donePaths;
    if (this.state.currentPoints.length > 0) {
      // Cache the shape object so that we aren't testing
      // whether or not it changed; too many components?
      newPaths.push(
        <Path
          key={this.state.currentMax}
          d={this.state.drawing.pointsToSvg(this.state.currentPoints)}
          stroke={this.props.color}
          strokeWidth={this.props.strokeWidth}
          fill="none"
        />
      );
    }

    this.state.drawing.addGesture(this.state.currentPoints);

    this.setState({
      currentPoints: [],
      currentMax: this.state.currentMax + 1
    });

    this.props.setDonePaths(newPaths);
  }

  _onLayoutContainer = (e) => {
    this.state.drawing.setOffset(e.nativeEvent.layout);
  }

  render() {
    return (
      <View
        onLayout={this._onLayoutContainer}
        style={[
          styles.drawContainer,
          this.props.containerStyle,
          { width: this.props.width, height: this.props.height }
        ]}
      >
        <ImageBackground
                        source={{ uri: this.props.filePath.uri }}
                        style={{ flex:1, justifyContent:'center',alignContent:'center' }}
                    >

        <View {...this._panResponder.panHandlers} style={{ flex:1 }}>
          <Svg
            style={styles.drawSurface}
          >
            <G>
              {this.props.donePaths}
              <Path
                key={this.state.currentMax}
                d={this.state.drawing.pointsToSvg(this.state.currentPoints)}
                stroke={this.props.color}
                strokeWidth={this.props.strokeWidth - 1}
                strokeOpacity={1}
              />
            </G>
          </Svg>

          {this.props.children}
        </View>
        </ImageBackground>
      </View>
    );
  }
}

let styles = StyleSheet.create({
  drawContainer: {
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 1
  },

  drawSurface: {
    backgroundColor: 'transparent',
    flex:1
  }
});
