import React from 'react';
import { Animated, StyleSheet, View, TouchableOpacity } from 'react-native';
import Colors from '../constants/Colors';
import { SCALE } from '../services/scale';

class Footer extends React.Component {
  scaleInAnimated = new Animated.Value(0);
  scaleOutAnimated = new Animated.Value(0);

  _renderOptions() {
    
  }

  render() {
    const allColors = Object.keys(Colors);
    return (
      <View style={styles.footerContainer}>
    
          <TouchableOpacity
            key={0}
            onPressIn={() => {this.props.onPressIn(this.scaleInAnimated)}}
            onPressOut={() => {this.props.onPressOut(this.scaleInAnimated)}}
            style={[styles.btnoption,SCALE.getScaleTransformationStyle(this.scaleInAnimated), { backgroundColor: Colors[0] == '#00000000'? 'yellow':Colors[0]}]}
            onPress={() => this.props.onPress(Colors[0])}
          />

          <TouchableOpacity
            key={1}
            onPressIn={() => {this.props.onPressIn(this.scaleInAnimated)}}
            onPressOut={() => {this.props.onPressOut(this.scaleInAnimated)}}
            style={[styles.btnoption,SCALE.getScaleTransformationStyle(this.scaleInAnimated), { backgroundColor: Colors[1] == '#00000000'? 'yellow':Colors[1]}]}
            onPress={() => this.props.onPress(Colors[1])}
          />
      </View>
    );
  }
}

let styles = StyleSheet.create({
  footerContainer: {
    flex:1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    backgroundColor: '#FFF',
    borderBottomWidth: 1,
    borderColor: '#DDD',
    paddingVertical: 5,
    paddingHorizontal: 10,
    paddingTop: 20,
    padding: 18,
  },

  btnoption: {
    flex:1,
    width: 20,
    height: 20,
    borderRadius: 10,
    marginVertical: 5,
    marginHorizontal: 10
  }
});

export default Footer;
