import React from 'react';
import { Animated, Dimensions, Image, View,
  Text, TouchableOpacity, StyleSheet } from 'react-native';
// import { takeSnapshotAsync } from 'exponent';
import Colors from '../constants/Colors';
import Header from '../components/Header';
import CanvasView from '../components/CanvasView';
import { SCALE } from '../services/scale';
import ImagePicker from 'react-native-image-picker';
import { Path } from 'react-native-svg';
export default class CanvasViewScreen extends React.Component {
  scaleInPencil = new Animated.Value(0);
  scaleOutPencil = new Animated.Value(0);
  scaleInEraser = new Animated.Value(0);
  scaleOutEraser = new Animated.Value(0);

  constructor(props, context) {
    super(props, context);

    this.state = {
      results: [],
      color: Colors.color1,
      strokeWidth: 4,
      donePaths: [],
      redoPaths:[],
      filePath: {},
    };

    this._undo = this._undo.bind(this);
    this._setDonePaths = this._setDonePaths.bind(this);
  }

  componentDidMount(){
    this.chooseFile()
  }

  chooseFile = () => {
      var options = {
          title: 'Select Image',
          customButtons: [
              { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
          ],
          storageOptions: {
              skipBackup: true,
              path: 'images',
          },
  };

  ImagePicker.showImagePicker(options, response => {
          if (response.didCancel) {
              console.log('User cancelled image picker');
          } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
              alert(response.customButton);
          } else {
              let source = response;
              this.setState({
                  filePath: source,
              });
          }
      });
  };

  _cancel = () => {
    this.setState({ results: [],
      color: Colors.color1,
      strokeWidth: 4,
      donePaths: [],
      redoPaths:[],
      filePath: {}, });
  }

  _undo = () => {
    if(this.state.donePaths.length == 0){
      return
    }
    console.log(this.state.donePaths)
    let removePath = this.state.donePaths[this.state.donePaths.length-1]
    console.log(removePath)

    this.setState({ 
      donePaths: this.state.donePaths.slice(0, -1),
      redoPaths:[...this.state.redoPaths, removePath]
    });
  }

  _redo = () => {
    if(this.state.redoPaths.length == 0){
      return
    }
    let addPreviousItem = this.state.redoPaths[this.state.redoPaths.length-1]
    let donePathsArr = []
    let tempPathsArr = [...this.state.donePaths]
      let obj = addPreviousItem
      if(obj.props != undefined)
      {
        tempPathsArr.push(<Path
          key={tempPathsArr.length}
          d={obj.props.d}
          stroke={obj.props.stroke}
          strokeWidth={obj.props.strokeWidth}
          fill="none"
        />)
        console.log("tempPathsArr")
        console.log(tempPathsArr)
      }

    // }
    this.setState({ 
      donePaths:[...tempPathsArr],
      redoPaths:this.state.redoPaths.slice(0, -1)
    });
  }

  _save = async () => {
    
    console.log("save image code here")
  }

  _setDonePaths = (donePaths) => {
    this.setState({ 
      donePaths:[...donePaths]
     });
  }

  _changeColor = (color, strokeWidth) => {    
    this.setState({ 
      color:color,
      strokeWidth:strokeWidth 
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Header
            save={this._save}
            undo={this._undo}
            cancel={this._cancel}
            redo={this._redo}
            isRemoveEnable={this.state.filePath.uri?false:true}
            isDoneEnable={this.state.filePath.uri?false:true}
            isUndoEnable={this.state.donePaths.length?false:true}
            isRedoEnable={this.state.redoPaths.length?false:true}

          />
        </View>
        <View style={styles.canvas}>
            {this.state.filePath.uri ? <CanvasView
              ref={(view) => { this.viewRef = view; }}
              donePaths={this.state.donePaths}
              setDonePaths={this._setDonePaths}
              containerStyle={{ backgroundColor: 'white', flex:1 }}
              width={Dimensions.get('window').width}
              height={Dimensions.get('window').width}
              color={this.state.color}
              strokeWidth={this.state.strokeWidth}
              filePath={this.state.filePath}
            />:<TouchableOpacity
                  style={styles.imageplaceholder}
                  onPress={() => {this.chooseFile()}}
                >
                  <Text style={styles.txtplaceholder}>Add Image</Text>
                </TouchableOpacity>}
        </View>
        <View style={styles.footer}>
          <View style={styles.footerContainer}>
            <TouchableOpacity
              key={0}
              onPressIn={() => {SCALE.pressInAnimation(this.scaleInPencil);}}
              onPressOut={() => {SCALE.pressOutAnimation(this.scaleInPencil);}}
              style={[styles.btnoption,SCALE.getScaleTransformationStyle(this.scaleInPencil)]}
              onPress={() => this._changeColor('black', 4)}
            >
              <Image source={require('../assets/pencil.png')}/>
            </TouchableOpacity>

            <TouchableOpacity
              key={1}
              onPressIn={() => {SCALE.pressInAnimation(this.scaleInEraser);}}
              onPressOut={() => {SCALE.pressOutAnimation(this.scaleInEraser);}}  
              onPress={() => this._changeColor('white', 16)}
              style={[styles.btnoption,SCALE.getScaleTransformationStyle(this.scaleInEraser)]}
            >
              <Image source={require('../assets/eraser.png')}/>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}


let styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white'
  },

  header:{
    flex:0.5,    
  },
  canvas:{
    flex:9
  },  

  footer: {
    flex:1,
    color: '#555',
    fontSize: 12,    
    bottom: 5,
    right: 10
  },
  footerContainer: {
    flex:1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderColor: '#DDD',
    paddingVertical: 5,
    paddingHorizontal: 10,
    paddingTop: 20,
    padding: 18,
  },

  btnoption: {
    flex:1,
    width: 20,
    borderRadius: 10,
    marginVertical: 5,
    marginHorizontal: 10
  },
  imageplaceholder:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'    
  },
  txtplaceholder:{
    fontSize:26,    
  }
});
